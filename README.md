# Projet RD 5: Création automatique des cartes heuristiques

Membres du groupe:
* Lola AIGUEPERSE
* Faïza BOUMALLOUK
* Lydie JEANTY
* Oumaima CHAMMAKHI

## Description
Le projet consiste à créer des cartes mentales automatiquement à partir de texte saisi par un utilisateur. Pour cela, nous allons développer une application web.

Nos fonctionnalités:
* générer la carte mentale en temps réel: on pourra saisir des éléments qui seront retranscrits sur la carte
* pouvoir exporter sa carte (SVG, PNG)
* proposer plusieurs "langages" pour la saisie des éléments (JSON, texte brut)
* donner la possibilité à l'utilisateur de choisir une couleur pour chaque élément saisi

## Documentation
Un [wiki](https://gitlab.com/banoufaiza/projet-rd/-/wikis/home) est à disposition. Vous y trouverez des explications pour l'installation et l'utilisation de l'application.

## Technologies et librairies utilisées
* Côté client:
    * Javascript
    * [GoJS](https://gojs.net/latest/index.html) pour la création de carte heuristique
    * HTML/CSS
    * Bootstrap
* Côté serveur:
    * [Node.js](https://nodejs.org/)
    * [Express.js](https://expressjs.com/)
    * [spaCy](https://spacy.io/)
    * Python 3

## Docker
Architecture faite avec [Docker](https://www.docker.com/) avec la présence de deux containers: 
1. Front sur le port 80
1. Serveur Node.js sur le port 3000

## Installation
Pré-requis: avoir Docker installé et lancé
1. Récupérer le projet Git: `git clone https://gitlab.com/banoufaiza/projet-rd.git`
1. Se déplacer dans le projet: `cd projet-rd`
1. Création des images Docker: `docker-compose -f docker-compose.yml build`
1. Lancer le container Docker: `docker-compose -f docker-compose.yml up -d`
1. Eteindre le container Docker: `docker-compose -f docker-compose.yml down`


