

var scrollSpy = new bootstrap.ScrollSpy(document.body, {
    target: '#navbar-example2'
})

var modelJson = {};

/* A chaque saisie de l'utilisateur pour le JSON, il y aura un aperçu */
$('#saisieTexteJson').on('input', function() {
    try {
        var obj = JSON.parse($('#saisieTexteJson').val());
        var text = JSON.stringify(obj, undefined, 3); // indentation à 3
        let html = '<pre>' + text + '</pre>';
        $('#apercuJson').html(html);
        $('#apercuJson').removeClass("fw-bold text-danger text-center");
        $('#btnJson').removeClass("disabled");
    } catch(error) {
        $('#apercuJson').html("Format JSON invalide");
        $('#apercuJson').addClass("fw-bold text-danger text-center");
        $('#btnJson').addClass("disabled");
    } 
});

initMindMap();

// Formulaire du texte saisi en JSON
$('#formJson').on('submit', function(event) {
    event.preventDefault();
    /* Validations de la saisie de texte JSON */
    let texteSaisi = $('#saisieTexteJson').val();
    if(!texteSaisi) {
        $('#apercuJson').html("Format JSON invalide");
        $('#apercuJson').addClass("fw-bold text-danger text-center");
        $('#btnJson').addClass("disabled");
        return;
    }
    var obj = JSON.parse(texteSaisi);
    if(!obj.racine) {
        $('#apercuJson').html("Veuillez spécifier une racine");
        $('#apercuJson').addClass("fw-bold text-danger text-center");
        $('#btnJson').addClass("disabled");
        return;
    }

    /* Création du modèle de la carte heuristique */
    modelJson = {
        "class": "go.TreeModel",
        "nodeDataArray": [],
    };
    modelJson.nodeDataArray.push({
        "key": 0,
        "text": obj.racine.texte,
    });

    modelJson.linkDataArray = [];
    let result = [];
    if(obj.racine.branches) {
        result = ajouterBranches(obj.racine.branches, 0, "black");
    }
    result.forEach(function(item) {
        modelJson.nodeDataArray.push(item);
        modelJson.linkDataArray.push({
            from:item.parent,
            to:item.key,
            lien:item.ontologie
        });
    });
    
    myDiagram.model = go.Model.fromJson(modelJson);
    checkOntologie();
});

function ajouterBranches(obj, indexParent, couleurParent) {
    let branches = [];
    obj.forEach(function(item, index) {
        let key = indexParent == 0 ? index+1 : indexParent + "" + index+1;
        branches.push({
            "parent": indexParent,
            "key": key,
            "brush": item.couleur ? item.couleur : couleurParent,
            "text": item.texte,
            "ontologie": item.lien
        });
        if(item.branches) {
            let arr = ajouterBranches(item.branches, key, item.couleur);
            branches = branches.concat(arr);
        }
    });
    return branches;
}

$(document).ready(function() {
    let template = {
        "racine": {
        "texte": "MindMap",
        "branches": [
        {
        "texte":"Getting more time", 
        "couleur":"skyblue",
        "branches": [
        { "texte": "Delegate"},
        { "texte": "Simplify", "lien":"by"},
        {"texte":"Wake up early", "lien":"by"}
        ]
        },
        { "texte": "More effective use",
        "couleur":"darkseagreen",
        "branches": [
        {"texte":"Planning","lien":"by", "branches":[{"texte":"Priorities", "lien":"set up"}, {"texte":"Ways to focus"}]}, {"texte":"Goals", "lien":"through"}]},
        { "texte": "Key issues", "couleur":"coral"}
        ]
        }
        };
    let text = JSON.stringify(template, undefined, 3);
    $('#templateJSON').html(text);


    $('#exportMap').click(function(e){
        $('#exampleModal').show();
        e.preventDefault();
    })

    $('#formExportMap').submit(function(e){
        let formatExport = $('input[name=format]:checked', '#formExportMap').val();
        if(formatExport=="svg"){
            var svg = myDiagram.makeSvg();
            var svgstr = new XMLSerializer().serializeToString(svg);
            var blob = new Blob([svgstr], { type: "image/svg+xml" });
            myCallback(blob,"svg");
        }else if(formatExport=="png"){
            var blob = myDiagram.makeImageData({ background: "white", returnType: "blob", callback: myCallback });
        }
        e.preventDefault();
    });
});

// Permet d'exporter la carte heuristique
function myCallback(blob, extension) {
    var url = window.URL.createObjectURL(blob);
    if(extension==null){
        extension= "png";
    }
    var filename = "MyMindMap."+extension;

    var a = document.createElement("a");
    a.style = "display: none";
    a.href = url;
    a.download = filename;

    // IE 11
    if (window.navigator.msSaveBlob !== undefined) {
      window.navigator.msSaveBlob(blob, filename);
      return;
    }

    document.body.appendChild(a);
    requestAnimationFrame(function() {
      a.click();
      window.URL.revokeObjectURL(url);
      document.body.removeChild(a);
    }); 
}

// Switch entre carte heuristique et ontologie
$("#switchTypeCarte").on('change', function(event) {
    event.preventDefault();
    checkOntologie();
});

function checkOntologie(){
    if ($("#switchTypeCarte").is(':checked')) {
        // Ontologie
        $("#switchTypeCarte").attr('value', 'true');
        myDiagram.model = new go.GraphLinksModel(modelJson.nodeDataArray, modelJson.linkDataArray);

        // afficher les liens et leurs textes
        myDiagram.linkTemplate = $$(go.Link,
            {
                curve: go.Link.Bezier,
                fromShortLength: -2,
                toShortLength: -2,
                selectable: false
            },
            $$(go.Shape, { strokeWidth: 3 }, new go.Binding("stroke", "toNode", function(n) {
                if (n.data.brush) return n.data.brush;
                return "black";
            }).ofObject()),
            $$(go.Panel, "Auto",
            $$(go.Shape,  // the label background, which becomes transparent around the edges
              {
                fill: $$(go.Brush, "Radial", { 0: "#dae4e4", 0.3: "#dae4e4", 1: "#dae4e4" }),
                stroke: null
              }),
            $$(go.TextBlock,  // the label text
              {
                textAlign: "center",
                font: "10pt helvetica, arial, sans-serif",
                stroke: "#555555",
                margin: 4
              },
              new go.Binding("text", "lien"))
            )
        );


    } else {
        // Carte heuristique
        $("#switchTypeCarte").attr('value', 'false');

        // afficher les liens
        myDiagram.linkTemplate = $$(go.Link,
            {
                curve: go.Link.Bezier,
                fromShortLength: -2,
                toShortLength: -2,
                selectable: false
            },
            $$(go.Shape, { strokeWidth: 3 }, new go.Binding("stroke", "toNode", function(n) {
                if (n.data.brush) return n.data.brush;
                return "black";
            }).ofObject())
        );
        myDiagram.model = go.Model.fromJson(modelJson);
    }
}

// Formulaire texte brut
$('#formTexteBrut').on('submit', function(event) {
    event.preventDefault();
    $("#contenuModal").html('<strong>Analyse du texte en cours...</strong><div class="spinner-border ms-auto" role="status" aria-hidden="true"></div>');
    let texteBrut = $('#saisieTexteBrut').val();

    modelJson = {
        "class": "go.TreeModel",
        "nodeDataArray": [],
    };
    modelJson.nodeDataArray.push({
        "key": 0,
        "text": "Mind map",
    });

    $.ajax({
        type: 'POST',
        url: "http://localhost:3000",
        data: { texte : texteBrut },
        dataType: 'json',
        success: function (data) {
            /* Création du modèle de la carte heuristique */
            modelJson = {
                "class": "go.TreeModel",
                "nodeDataArray": [],
            };
            modelJson.nodeDataArray.push({
                "key": 0,
                "text": "Carte Heuristique",
            });
            modelJson.linkDataArray = [];
            data.forEach(function(item,index){
                let key = index+1;
                let couleur = '#'+(0x1000000+Math.random()*0xffffff).toString(16).substring(1,7);
                modelJson.nodeDataArray.push({
                    "parent": 0,
                    "key":key,
                    "text": item.parent,
                    "ontologie":item.ontologie,
                    "brush":couleur
                });
                modelJson.linkDataArray.push({
                    from:0,
                    to:key,
                    lien:""
                });
                modelJson.nodeDataArray.push({
                    "parent": key,
                    "key":key+""+1,
                    "text": item.enfant,
                    "brush": couleur
                });
                modelJson.linkDataArray.push({
                    from:key,
                    to:key+""+1,
                    lien:item.ontologie
                });
            })
            myDiagram.model = go.Model.fromJson(modelJson);
            checkOntologie();
            $("#contenuModal").html('<strong>Analyse terminée ! </strong><div class="ms-auto text-success"><i class=" bi fs-3 bi-check-circle-fill"></i></div>')
        },
        error: function (xhr) {
            $("#contenuModal").html('<strong>'+xhr.responseJSON.error+'</strong><div class="ms-auto text-danger"><i class="bi fs-3 bi-x-circle-fill"></i></div>')

        }
    });
});