const express           = require('express'),
      cors              = require('cors'),
      { PythonShell }   = require('python-shell'),
      app               = express();

// ------------------------
// CONFIG EXPRESS
// ------------------------
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// ------------------------
// CONFIG SERVEUR
// ------------------------
const httpServer = require("http").createServer(app);

// ------------------------
// ROUTES
// ------------------------
app.post('/', (req, res) => {
    let options = {
        mode: 'text',
        pythonPath: '/usr/bin/python3',
        pythonOptions: ['-u'], // get print results in real-time
        scriptPath: './app/scripts/',
        args: [req.body.texte]
    };

    PythonShell.run('ontologie.py', options, function (err, results) {
        if (err) res.status(400).json({ error: "Erreur lors de l'analyse." });

        let resultsObj = results.filter(function(str) {
            let obj = JSON.parse(str);
            return obj.parent!="" && obj.enfant!="";
        }).map(function(str){
            let obj = JSON.parse(str);
            return obj;
        });

        if(resultsObj.length>0){
            res.status(200).json(resultsObj);
        }
        else {
            res.status(400).json({ error: "Aucun lien entre les mots n'a été trouvé." });
        }
    });
});


// ------------------------
// START LISTENING SERVER
// ------------------------
httpServer.listen(3000, function() {
    console.info('HTTP server started on port 3000');
});